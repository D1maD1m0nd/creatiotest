define("qrtBudgetWidget", ["ext-base", "terrasoft", "sandbox", "BaseFiltersGenerateModule", "qrtBudgetWidgetResources","css!qrtBudgetWidget", "LookupUtilities"], 
		function(Ext, Terrasoft, sandbox, BaseFiltersGenerateModule, resources, LookupUtilities) {
 
		 function getViewModel() {
			 return Ext.create("Terrasoft.BaseViewModel", {
				 entitySchema: "qrtBudget",
				 methods: {
					 getChart: function(key) {
						 sandbox.publish("GenerateChart", key);
					 },
					 load: function() {}
				 }
			 });
		 }
		 function generateMainView(renderTo) {
			 var resultConfig = Ext.create("Terrasoft.Container", {
				 id: "myContainer",
				 selectors: {
					 wrapEl: "#myContainer"
				 },
				 renderTo: renderTo
			 });
			 return resultConfig;
		 }
		 function fillDom() {
			 //var htmlAdded = "<p><textarea></textarea></p>";
			 /* Формирование таблицы */
			 var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "qrtBudgetDetail"});
			 esq.addColumn("Id");
			 esq.addColumn("CreatedOn");//Дата создания
			 esq.addColumn("qrtName");//Номер
			 esq.addColumn("qrtBudget");//Бюджет (использовать для фильтров)
			 esq.addColumn("qrtBudget.qrtBranchOffice", "BranchOffice");//Бюджет.Филиал
			 esq.addColumn("qrtCostItem");//Cтатья затрат
			 esq.addColumn("qrtLineItemGroup");//Группа статей затрат
			 esq.addColumn("qrtAdjustment");//Корректировка в б.в.
			 esq.addColumn("qrtSumFact");//Сумма Факт в б.в.
			 esq.addColumn("qrtSumPlan");//Сумма План в б.в.

			 esq.getEntityCollection(function (result) {
				 if (!result.success) {
					 // обработка/логирование ошибки, например
					 this.showInformationDialog("Ошибка запроса данных:\n" + "\nErrorCode: " + result.errorInfo.errorCode + "\nMessage: " + result.errorInfo.message);
					 return;
				 }
				 var blocks = "";
				 var officeList = [];
				 var groupList = [];
				 var officeData = [];
				 var planObj = {};
				 
				 result.collection.each(function(item) {
					 
					 var office = item.get("BranchOffice");
					 var group = item.get("qrtLineItemGroup");
					 var costItem = item.get("qrtCostItem");
					 var plan = item.get("qrtSumPlan");
					 
					 /* Если группа офисов не создана создаем */
					 if (!planObj[office.displayValue]){
						 planObj[office.displayValue] = {};
					 }
					 
					 /* Если группа статей не создана создаем */
					 if (!planObj[office.displayValue][group.displayValue]){
						 planObj[office.displayValue][group.displayValue] = {};
					 }
					 
					 planObj[office.displayValue][group.displayValue][costItem.displayValue] = (!planObj[office.displayValue][group.displayValue][costItem.displayValue]) ? plan : plan++;
					 
				 }, this);
				 
				 window.console.log("after", planObj);
				 window.console.log("результат: ", planObj["Филиал г. Казань"]["BTL (Канальная стратегия)"]["Ивенты"]);
				 window.console.log(Object.keys(planObj).length);
				 
				 for (const [officeName, officeValue] of Object.entries(planObj)) {
					 var isFirst = true;
					 var lines = "";
					 var test = 0;
					 for (const [groupName, groupValue] of Object.entries(officeValue)) {
						 var cells = "";
						 for (const [cellName, cellValue] of Object.entries(groupValue)) {
							 let tag = (cells !== "") ? "<tr>" : "";
							 
							 cells +=
								 tag +
									 "<td class='qrt-text'>" + cellName.replace('"', '\"') + "</td>" +
									 "<td scope='col'><input type='number' class='qrt-cell-plan' id='plan_0' name='plan_0' min='0' value='" + cellValue + "'></td>" +
									 "<td scope='col' class='qrt-cell'>22</td>" +
									 "<td scope='col' class='qrt-cell'>43</td>" +
									 "<td scope='col' class='qrt-cell'>72</td>" +
									 "<td scope='col' class='qrt-cell'>23</td>" +
									 "<td scope='col' class='qrt-cell'>33</td>" +
								 tag;
						 }
						 test += (Object.keys(groupValue).length == 2) ? Object.keys(groupValue).length : 0;
						 lines += "<tr><th rowspan='" + Object.keys(groupValue).length + "' scope='rowgroup' class='qrt-text'>" + groupName + "</th>" + cells + "<\tr>";
					 }
					 
					 
					 var sum = parseInt(Object.keys(officeValue).length) + parseInt(test) + 1;
					 window.console.log("test", test);
					 window.console.log("sum", sum);
					 blocks += "<tr>" +
							 "<th rowspan='" + sum + "' scope='rowgroup' class='qrt-text'>" + officeName + "</th>" +
							 lines +
							 "</tr>";
					 //break;
				 }
				 
				 var htmlAdded = 
					 "<button id='add' class='qrt-add-button'>ДОБАВИТЬ</button>" +
					 "<table border='1'; style='border-color:grey;'><tbody>" +
					 	"<tr><th colspan='9' class='qrt-text'>2022</th></tr>" +
						 "<tr>" +
					 		"<td colspan='3'></td>" +
					 		"<th colspan='3' scope='colgroup' class='qrt-text'>Апрель</th>" +
					 		"<th colspan='3' scope='colgroup' class='qrt-text'>Май</th>" +
						 "</tr>" +
					 	 "<tr>" +
					 		"<td colspan='3'></td>" +
					 		"<th scope='col' class='qrt-text'>Сумма План</th>" +
					 		"<th scope='col' class='qrt-text'>Корректировка</th>" +
							"<th scope='col' class='qrt-text'>Сумма Факт</th>" +
							"<th scope='col' class='qrt-text'>Сумма План</th>" +
					 		"<th scope='col' class='qrt-text'>Корректировка</th>" +
					 		"<th scope='col' class='qrt-text'>Сумма Факт</th>" +
						 "</tr>" +
					 	/* Первый филиал */
					 	 blocks +
					 "</tbody></table>";
				 Ext.get("myContainer").setHTML(htmlAdded);
				 /* Подписка на изменение полей */				   
				 subscribersEvent(this);
			 }, this);

		 }
		 function subscribersEvent() {
			 /* Поля */
			 document.getElementById("plan_0").addEventListener('change', function() {
				 let elem = document.getElementById('plan_0').value;
				 window.console.log("plan_0", elem);
			 });
			 /* Кнопки */
			 document.getElementById("add").addEventListener('click', function() {
				 var config = {
					 entitySchemaName: "qrtBudget",
					 multiSelect: false,
					 columnName: "Name",
				 };
				 
				 Terrasoft.LookupUtilities.open({
					 "lookupConfig": config,
					 "sandbox": sandbox,
					 "keepAlive": config.keepAlive,
					 "lookupModuleId": config.lookupModuleId,
					 "lookupPageName": config.lookupPageName,
				 }, function(arg){
					 var country = arg.selectedRows.collection.items[0];
					 window.console.log("country", country);
					 return;
				 }, this);
			 });
		 }
		 var render = function(renderTo) {

			 var viewConfig = generateMainView(renderTo);
			 var viewModel = getViewModel();

			 fillDom();

			 viewConfig.bind(viewModel);
			 //viewConfig.render(renderTo);
		 };

		 return {
			 schema: "qrtBudget",
			 methods: {},
			 userCode: function() {},
			 init: function() {},
			 filterChanged: function(filter, eOpts) {},
			 render: render
		 };
     }
);